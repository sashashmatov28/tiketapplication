package org.java.application.ticket;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.AuthService;
import org.java.application.ticket.storage.TicketStorage;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TicketService {
    private final TicketStorage ticketStorage;
    private final AuthService authService;
    private Ticket tmpTicket;

    public void createTicket(Ticket ticket) {
        if (isNullOrEmpty(ticket.getCreator()) || isNullOrEmpty(ticket.getAssigned()) ||
                isNullOrEmpty(ticket.getDescription()) || isNullOrEmpty(ticket.getComment()) ||
                isNullOrEmpty(ticket.getStatus()) || isNullOrEmpty(ticket.getId())) {
            throw new IllegalArgumentException("Some fields not much rule");
        }
        boolean exists = ticketStorage.findByNumber(ticket.getId()).isPresent();
        if (exists) {
            throw new IllegalArgumentException("Ticket already exist");
        }
        ticketStorage.save(ticket);
        tmpTicket = ticket;
    }

    public Ticket getTicket() {
        return tmpTicket;
    }

    private boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public void changeTicketStatus(String id, String status) {

        ticketStorage.changeStatus(id, status);
    }

    public List<Ticket> findTickets(String by) {
        List<Ticket> tickets;
        if (by.equals("all")) {
            tickets = ticketStorage.getAll().collect(Collectors.toList());
        } else if (by.equals("my tickets")) {
            tickets = ticketStorage.getAll()
                    .filter(u -> u.getAssigned().equals(authService.getUsername())).collect(Collectors.toList());
        } else {
            tickets = ticketStorage.getAll().filter(u -> u.getId().equals(by)).collect(Collectors.toList());
        }
        return tickets;
    }
}