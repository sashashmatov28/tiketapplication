package org.java.application.ticket;

import lombok.RequiredArgsConstructor;
import org.java.application.infrastructure.menu.ui.ViewHelper;
import org.java.application.ticket.enumstatus.Status;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class TicketView {
    private final ViewHelper viewHelper;

    public Ticket readTicket() {
        String creator = viewHelper.readString("Enter your creator: ");
        String assigned = viewHelper.readString("Enter your assigned: ");
        String description = viewHelper.readString("Enter your description: ");
        String comment = viewHelper.readString("Enter your comment: ");
        String id = viewHelper.readString("Enter your id: ");
        return new Ticket(creator, assigned, description, comment, Status.NEW.getStatus(), id);
    }

    public List<String> selectStatus() {
        List<String> list = new ArrayList<>();
        String id = viewHelper.readString("Enter your id: ");
        String status = viewHelper.readString("Enter new status: ");
        list.add(id);
        switch (status) {
            case "New":
                list.add(Status.NEW.getStatus());
                break;
            case "In progress":
                list.add(Status.INPROGRESS.getStatus());
                break;
            case "Success":
                list.add(Status.SUCCESS.getStatus());
                break;
            default:
                throw new IllegalArgumentException("Incorrect status");
        }
        return list;
    }

    public String getFinder() {
        return viewHelper.readString("Select how search ticket: 'all', 'my tickets' or id of ticket ");
    }


    public void showError(String error) {
        viewHelper.showError(error);
    }
}
