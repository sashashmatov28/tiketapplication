package org.java.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.AuthService;
import org.java.application.infrastructure.menu.MenuItem;
import org.java.application.ticket.Ticket;
import org.java.application.ticket.TicketService;
import org.java.application.ticket.TicketView;

@RequiredArgsConstructor
public class CreateTicketMenuItem implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;
    private final TicketView ticketView;

    @Override
    public String getName() {
        return "Create ticket";
    }

    @Override
    public void run() {
        Ticket ticket = ticketView.readTicket();
        try {
            ticketService.createTicket(ticket);
        } catch (IllegalArgumentException e) {
            ticketView.showError(e.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
