package org.java.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.AuthService;
import org.java.application.auth.User;
import org.java.application.auth.UserView;
import org.java.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class RegisterMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Register";
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }

    @Override
    public void run() {
        User user = userView.readUser();
        try {
            authService.register(user);
        } catch (IllegalArgumentException e) {
            userView.showError(e.getMessage());
        }
    }
}
