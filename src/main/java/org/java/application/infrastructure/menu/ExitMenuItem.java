package org.java.application.infrastructure.menu;

public class ExitMenuItem implements MenuItem {

    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void run() {

    }

    @Override
    public boolean isFinal() {
        return true;
    }

}
